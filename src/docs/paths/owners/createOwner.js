module.exports = {
    post: {
        tags: ['owners'],
        description: "Registra de un propietario",
        operationId: "createOwner",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/createOwner'
                    }
                }
            }
        },
        responses: {
            'API_OW_200': {
                description: 'Propietario registrado.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createOwner'
                        }
                    }
                }
            },
            'API_OW_403': {
                description: 'Hubo un fallo al registrar el piso. Intente de nuevo.'
            },
            'API_OW_500': {
                description: 'Ha ocurrido un error en crear un propietario.'
            }
        }
    }
}