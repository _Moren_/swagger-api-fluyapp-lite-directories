module.exports = {
    get: {
        tags: ['owners'],
        description: "Retorna el detalle acerca de un propietario",
        operationId: "viewDetailOnwer",
        parameters: [
            {
                in: 'params',
                name: '_id'
            }
        ],
        responses: {
            'API_OW_200': {
                description: 'Detalles del propietario.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createOwner'
                        }
                    }
                }
            },
            'API_OW_404': {
                description: 'No existe este dato del propietario.'
            },
            'API_OW_500': {
                description: 'Ha ocurrido un error en lisatr los propietarios.'
            }
        }
    }
}