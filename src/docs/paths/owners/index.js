const createOwner = require('./createOwner');
const listsOwner = require('./listsOwner');
const updateOwner = require('./updateOwner')
const viewDetailOnwer = require('./viewDetailOnwer');

module.exports = {
    '/owner-create/': {
        ...createOwner
    },
    '/owner-lists/': {
        ...listsOwner
    },
    '/owner-view/:_id': {
        ...viewDetailOnwer
    },
    '/owner-update/:_id': {
        ...updateOwner
    }
}