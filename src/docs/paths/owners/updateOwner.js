module.exports = {
    put: {
        tags: ['owners'],
        description: "Actualiza información de un propietario",
        operationId: "updateOwner",
        parameters: [
            {
                in: 'params',
                name: '_id'
            }
        ],
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/requestBodyOwnerUpdate'
                    }
                }
            }
        },
        responses: {
            'API_OW_200': {
                description: 'Propietario actualizado con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createOwner'
                        }
                    }
                }
            },
            'API_OW_404': {
                description: 'No hay registro con ese identificador.'
            },
            'API_OW_500': {
                description: 'Ha ocurrido un error en lisatr los propietarios.'
            }
        }
    }
}