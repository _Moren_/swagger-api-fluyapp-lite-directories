module.exports = {
    get: {
        tags: ['owners'],
        description: "Retorna todos los datos de los propietarios",
        operationId: "listsOwner",
        responses: {
            'API_OW_200': {
                description: 'Lista de propietarios.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createOwner'
                        }
                    }
                }
            },
            'API_OW_404': {
                description: 'Hasta el momento, no hay propietarios registrados'
            },
            'API_OW_500': {
                description: 'Ha ocurrido un error en lisatr los propietarios.'
            }
        }
    }
}