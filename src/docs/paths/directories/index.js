const listsDirectories = require('./listsDirectories');
const createDirectory = require('./createDirectory');
const listsByEntity = require('./listsByEntity');
const updateImgDirectory = require('./updateImgDirectory');
const viewDetailDirectory = require('./viewDetailDirectory');

module.exports = {
    '/directory-create/': {
        ...createDirectory
    },
    '/directory-lists': {
        ...listsDirectories
    },
    '/directory-list-entity/': {
        ...listsByEntity
    },
    '/directory-update-img/:_id': {
        ...updateImgDirectory
    },
    '/directory-view/:_id': {
        ...viewDetailDirectory
    }
}