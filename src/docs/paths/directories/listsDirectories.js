module.exports = {
    get: {
        tags: ['directories'],
        description: "Retorna todos los datos de los directorios sin filtro",
        operationId: "listsDirectories",
        responses: {
            'API_DIR_200': {
                description: 'Listado de directorios.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createOwner'
                        }
                    }
                }
            },
            'API_DIR_404': {
                description: 'Algo ocurrió al mostrar la información.'
            },
            'API_DIR_404': {
                description: 'Algo pasó. Acuda a su administrador.'
            }
        }
    }
}