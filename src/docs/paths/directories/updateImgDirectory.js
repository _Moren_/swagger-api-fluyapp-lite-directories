module.exports = {
    put: {
        tags: ['directories'],
        description: "Actualiza los datos de un directorio",
        operationId: "updateImgDirectory",
        parameters: [
            {
                in: 'params',
                name: '_id'
            }
        ],
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/requestBodyUpdate'
                    }
                }
            }
        },
        responses: {
            'API_DIR_200': {
                description: 'Directorios actualziado con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createCamera'
                        }
                    }
                }
            },
            'API_DIR_404': {
                description: 'Algo ocurrió al actualizar la información.',
            },
            'API_DIR_404': {
                description: 'Algo pasó. Acuda a su administrador.'
            }
        }
    }
}