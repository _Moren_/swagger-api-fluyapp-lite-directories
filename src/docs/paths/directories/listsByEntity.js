module.exports = {
    get: {
        tags: ['directories'],
        description: "Retorna valores específicos de los directorios en base al id de una entidad",
        operationId: "listsByEntity",
        parameters: [
            {
                in:'query',
                name:'flag'
            },
            {
                in:'query',
                name:'entity'
            }
        ],
        responses: {
            'API_DIR_200': {
                description: 'Listado de directorios por su entidad.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/responseListDirectories'
                        }
                    }
                }
            },
            'API_DIR_402': {
                description: 'Algo ocurrió al mostrar la información.'
            },
            'API_DIR_404': {
                description: 'Algo pasó. Acuda a su administrador.'
            }
        }
    }
}