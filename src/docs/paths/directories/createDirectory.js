module.exports = {
    post: {
        tags: ['directories'],
        description: "Registra los datos de un directorio",
        operationId: "createDirectory",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/requestBodyOwnerUpdate'
                    }
                }
            }
        },
        responses: {
            'API_DIR_200': {
                description: 'Datos del directorio creado con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createOwner'
                        }
                    }
                }
            },
            'API_DIR_404': {
                description:'Algo ocurrió al guardar la información.'
            },
            'API_DIR_404': {
                description: 'Algo pasó. Acuda a su administrador.'
            }
        }
    }
}