module.exports = {
    get: {
        tags: ['directories'],
        description: "Retorna los detalles de un directorio",
        operationId: "viewDetailDirectory",
        parameters: [
            {
                in: 'params',
                name: '_id'
            }
        ],
        responses: {
            'API_DIR_200': {
                description: 'Detalles del directorio.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createCamera'
                        }
                    }
                }
            },
            'API_DIR_404': {
                description: 'Algo ocurrió al mostrar la información.'
            },
            'API_DIR_404': {
                description: 'Algo pasó. Acuda a su administrador.'
            }
        }
    }
}