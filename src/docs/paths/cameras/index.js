const createCamera = require('./createCamera');
const updateFieldCamera = require('./updateFieldCamera');
const listsCameras = require('./listsCameras')
const listsByBranchFlag = require('./listsByBranchFlag');
const updateCamera = require('./updateCamera');
const viewDetailCamera = require('./viewDetailCamera');
const deleteCamera = require('./deleteCamera')

module.exports = {
    '/camera-create/': {
        ...createCamera
    },
    '/camera-update/:_id': {
        ...updateFieldCamera
    },
    '/camera-lists/': {
        ...listsCameras
    },
    '/camera-asociate-list': {
        ...listsByBranchFlag
    },
    '/camera-asociar/': {
        ...updateCamera
    },
    '/camera-view/:_id': {
        ...viewDetailCamera
    },
    '/camera-asociate-delete/': {
        ...deleteCamera
    }
}