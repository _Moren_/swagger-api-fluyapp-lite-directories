module.exports = {
    post: {
        tags: ['cameras'],
        description: "Retorna los valores de las cámaras por departamento, sucursal o servicio",
        operationId: "returnIdNameCamera",
        parameters: [
            {
                in:'query',
                name:'entity'
            }
        ],
        responses: {
            'API_CMR_200': {
                description: 'Listado de cámaras con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createCamera'
                        }
                    }
                }
            },
            'API_CMR_403': {
                description: 'Hubo un fallo al listar los datos de las cámaras. Intente de nuevo.'
            },
            'API_CMR_500': {
                description: 'Ha ocurrido un error intentar listar las cámaras.'
            }
        }
    }
}