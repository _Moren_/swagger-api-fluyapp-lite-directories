module.exports = {
    put: {
        tags: ['cameras'],
        description: "Actualiza los valores para  una cámara",
        operationId: "updateCamera",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/requestBodyUpdateCamera'
                    }
                }
            }
        },
        responses: {
            'API_CMR_200': {
                description: 'Cámara asociada con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createCamera'
                        }
                    }
                }
            },
            'API_CMR_403': {
                description: 'Algo pasó al actualizar los datos de la cámara.'
            },
            'API_CMR_500': {
                description: 'Ha ocurrido un error al actualizar los datos de una cámara.'
            }
        }
    }
}