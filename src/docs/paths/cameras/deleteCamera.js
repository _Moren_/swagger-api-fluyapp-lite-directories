module.exports = {
    put: {
        tags: ['cameras'],
        description: "Elimina los valores para  una cámara ya sea servicios, departamento o sucursales",
        operationId: "deleteCamera",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/requestBodyUpdateCamera'
                    }
                }
            }
        },
        responses: {
            'API_CMR_200': {
                description: 'Asociación eliminada con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createCamera'
                        }
                    }
                }
            },
            'API_CMR_403': {
                description: 'Hubo un error al desasociar los datos de la cámara. Intente de nuevo.'
            },
            'API_CMR_500': {
                description: 'Ha ocurrido un error al intentar eliminar una cámara.'
            }
        }
    }
}