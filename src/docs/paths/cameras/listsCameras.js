module.exports = {
    get: {
        tags: ['cameras'],
        description: "Retorna todos los valores de las cámaras",
        operationId: "listsCameras",
        parameters: [
            {
                in: 'query',
                name: 'entity'
            }
        ],
        responses: {
            'API_CMR_200': {
                description: 'Listado de cámaras.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createCamera'
                        }
                    }
                }
            },
            'API_CMR_403': {
                description: 'No hay cámara para mostrar.'
            },
            'API_CMR_500': {
                description: 'Ha ocurrido un error intentar listar las cámaras.'
            }
        }
    }
}