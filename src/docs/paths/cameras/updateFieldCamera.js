module.exports = {
    put: {
        tags: ['cameras'],
        description: "Actualiza los valores para  una cámara",
        operationId: "updateFieldCamera",
        parameters: [
            {
                in: 'params',
                name: '_id'
            }
        ],
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/requestBodyCameraUpdated'
                    }
                }
            }
        },
        responses: {
            'API_CMR_200': {
                description: 'Cámara actualizada con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createCamera'
                        }
                    }
                }
            },
            'API_CMR_403': {
                description: 'Hubo un error al actualizar los datos de la cámara. Intente de nuevo.'
            },
            'API_CMR_500': {
                description: 'Ha ocurrido un error en actualizar los datos de cámara.'
            }
        }
    }
}