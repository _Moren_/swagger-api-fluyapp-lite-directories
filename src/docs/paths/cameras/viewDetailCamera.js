module.exports = {
    get: {
        tags: ['cameras'],
        description: "Retorna los valores de una cámara",
        operationId: "viewDetailCamera",
        parameters: [
            {
                in: 'params',
                name: '_id'
            }
        ],
        responses: {
            'API_CMR_200': {
                description: 'Detalles de una cámaras.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createCamera'
                        }
                    }
                }
            },
            'API_CMR_403': {
                description: 'No hay cámara para mostrar.'
            },
            'API_CMR_500': {
                description:  'Ha ocurrido un error al intentar ver el detalle de una cámara.'
            }
        }
    }
}