module.exports = {
    post: {
        tags: ['cameras'],
        description: "Registra los valores para  una cámara",
        operationId: "createCamera",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/createCamera'
                    }
                }
            }
        },
        responses: {
            'API_CMR_200': {
                description: 'Cámara registradas con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createCamera'
                        }
                    }
                }
            },
            'API_CMR_403': {
                description: 'Hubo un fallo al ingresar los datos de a cámara. Intente de nuevo.'
            },
            'API_CMR_500': {
                description: 'Ha ocurrido un error en crear los datos de cámara.'
            }
        }
    }
}