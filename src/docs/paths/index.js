const cameras = require('./cameras/index');
const owners = require('./owners/index');
const directories = require('./directories/index');
const pregisters = require('./pregisters/index');
const pregisterVisit = require('./pregisters-visit/index');

module.exports = {
    paths: {
        ...cameras,
        ...owners,
        ...directories,
        ...pregisters,
        ...pregisterVisit
    }
}