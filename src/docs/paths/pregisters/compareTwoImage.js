module.exports = {
    post: {
        tags: ['pregisters'],
        description: "Hace la comparación de los rostros de quienes van a acceder a un sitio por medio de reconocimiento facial",
        operationId: "compareTwoImage",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/requestBodyFaceRecognition'
                    }
                }
            }
        },
        responses: {
            'API_PRG_200': {
                description: 'Hay coincidencia.'
            },
            'API_PRG_404': {
                description: 'No hay registros de esta persona.'
            },
            'API_PRG_403': {
                description: 'Hubo algo inesperado al comparar las imagenes'
            },
            'API_PRG_402': {
                description: 'No hay coincidencia.'
            }
        }
    }
}