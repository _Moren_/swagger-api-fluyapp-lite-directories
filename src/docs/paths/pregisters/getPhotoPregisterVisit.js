module.exports = {
    post: {
        tags: ['pregisters'],
        description: "Retorna la fotografía de un usuario",
        operationId: "getPhotoPregisterVisit",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/requestIdPhoto'
                    }
                }
            }
        },
        responses: {
            'API_PRG_200': {
                description: 'Fotorafía de visitante.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/responsePhoto'
                        }
                    }
                }
            },
            'API_PRG_404': {
                description: 'No existe este valor en los registros.'
            },
            'API_PRG_500': {
                description: 'Ha ocurrido un error al retornar la foto.'
            }
        }
    }
}