module.exports = {
    post: {
        tags: ['pregisters'],
        description: "Muestra los datos de personas que accedieron al sitio el día actual. Internamente toma la fecha actual.",
        operationId: "reportPregisterToday",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/requestBodyToReportToday'
                    }
                }
            }
        },
        responses: {
            'API_PRG_200': {
                description: 'Listado de los registrados.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/reponseBodyToReportToday'
                        }
                    }
                }
            },
            'API_PRG_404': {
                description: 'No hay regisros para la fecha actual.'
            },
            'API_PRG_500': {
                description: 'Ha ocurrido un error al generar el reporte.'
            }
        }
    }
}