module.exports = {
    post: {
        tags: ['pregisters'],
        description: "Retorna el historial completo de un usuario",
        operationId: "getHistoryPregisterVisit",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/requestBodyUserHistory'
                    }
                }
            }
        },
        responses: {
            'API_PRG_200': {
                description: 'Fotorafía de visitante.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/responseHistoryUser'
                        }
                    }
                }
            },
            'API_PRG_404': {
                description: 'No hay registro de un historial.'
            },
            'API_PRG_500': {
                description: 'Ha ocurrido un error al mostrar el historial.'
            }
        }
    }
}