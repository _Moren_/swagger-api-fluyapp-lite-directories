module.exports = {
    post: {
        tags: ['pregisters'],
        description: "Genera un reporte en formato .xsls según los parámetros dados. Internamente toma la fecha actual.",
        operationId: "generateExcelTodayReport",
        requestBody:{
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/requestBodyToReportTodayExcel'
                    }
                }
            }
        },
        responses: {
            'API_PRG_200': {
                description: 'Listado de los registrados.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/responseBodyToReportTodayExcel'
                        }
                    }
                }
            },
            'API_PRG_404': {
                description: 'No hay regisros para la fecha actual.'
            },
            'API_PRG_500': {
                description: 'Ha ocurrido un error al generar el reporte.'
            }
        }
    }
}