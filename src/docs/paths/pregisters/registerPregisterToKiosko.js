module.exports = {
    post: {
        tags: ['pregisters'],
        description: "Registra los datos personales de quien va a acceder a un sitio",
        operationId: "registerPregisterToKiosko",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/createPregister'
                    }
                }
            }
        },
        responses: {
            'API_PRG_200': {
                description: 'Preregistro realizado con éxito.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/createPregister'
                        }
                    }
                }
            },
            'API_PRG_403': {
                description: 'Hubo un fallo al hacer el preregistro. Intente de nuevo.'
            },
            'API_PRG_500': {
                description: 'Ha ocurrido un error en crear un preregistro.'
            }
        }
    }
}