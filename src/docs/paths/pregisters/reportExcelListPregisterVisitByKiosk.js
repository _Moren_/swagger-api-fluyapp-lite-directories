module.exports = {
    post: {
        tags: ['pregisters'],
        description: "Muestra los datos de personas que accedieron al sitio entre fechas en un archivo excel",
        operationId: "reportExcelListPregisterVisitByKiosk",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/requestBodyVisitKiosk'
                    }
                }
            }
        },
        responses: {
            'API_PRG_200': {
                description: 'Listado de visitantes por kiosco.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/responseBodyVisitKiosk'
                        }
                    }
                }
            },
            'API_PRG_404': {
                description: 'No hay registros.'
            },
            'API_PRG_500': {
                description: 'Ha ocurrido un error al generar el reporte.'
            }
        }
    }
}