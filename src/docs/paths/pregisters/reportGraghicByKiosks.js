module.exports = {
    post: {
        tags: ['pregisters'],
        description: "Retorna conteo de cuantas personas han pasado por un determinado kiosco y un url para descarga el reporte",
        operationId: "reportGraghicByKiosks",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/requestBodyVisitKioskGraphic'
                    }
                }
            }
        },
        responses: {
            'API_PRG_200': {
                description: 'Reporte según las vistas por kiosco.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/responseBodyVisitKioskGraphic'
                        }
                    }
                }
            },
            'API_PRG_404': {
                description: 'No se pudo generar el reporte. Intente de nuevo.',
            },
            'API_PRG_500': {
                description: 'Ha ocurrido un error al generar el reporte.'
            }
        }
    }
}