module.exports = {
    post: {
        tags: ['pregisters'],
        description: "Genera un archivo excel para reportes. Internamente toma la fecha actual.",
        operationId: "generateReportLocalByDateExcel",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/reuqestBodyReportLocalByDateExcel'
                    }
                }
            }
        },
        responses: {
            'API_PRG_200': {
                description: 'Listado de los registrados.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/responseBodyToReportTodayExcel'
                        }
                    }
                }
            },
            'API_PRG_404': {
                description: 'No hay regisros para la fecha actual.'
            },
            'API_PRG_500': {
                description: 'Ha ocurrido algo inesperado al generar el reporte.'
            }
        }
    }
}