const registerPregisterToKiosko = require('./registerPregisterToKiosko');
const compareTwoImage = require('./compareTwoImage');
const reportPregisterToday = require('./reportPregisterToday');
const generateExcelTodayReport = require('./generateExcelTodayReport');
const reportPregisterByLocal = require('./reportPregisterByLocal');
const reportToExcelByDate = require('./reportToExcelByDate');
const getPhotoPregisterVisit = require('./getPhotoPregisterVisit');
const reportVisitTower = require('./reportVisitTower');
const reportGraghicByKiosks = require('./reportGraghicByKiosks');
const listPregisterVisitByKiosk = require('./listPregisterVisitByKiosk');
const getHistoryPregisterVisit = require('./getHistoryPregisterVisit');
const reportExcelListPregisterVisitByKiosk = require('./reportExcelListPregisterVisitByKiosk');
const reportPregisterLocalByDate = require('./reportPregisterLocalByDate');
const generateReportLocalByDateExcel = require('./generateReportLocalByDateExcel');

module.exports = {
    '/register-visite-kiosk/': {
        ...registerPregisterToKiosko
    },
    '/pregister-compare-face/': {
        ...compareTwoImage
    },
    '/pregister-visit-today/': {
        ...reportPregisterToday
    },
    '/generate-report-today': {
        ...generateExcelTodayReport
    },
    '/pregister-visit-local/': {
        ...reportPregisterByLocal
    },
    '/pregister-visit-date/': {
        ...reportPregisterLocalByDate
    },
    '/generate-report-by-date': {
        ...generateReportLocalByDateExcel
    },
    '/pregister-report': {
        ...reportToExcelByDate
    },
    '/pregister-photo/': {
        ...getPhotoPregisterVisit
    },
    '/pregister-history/': {
        ...getHistoryPregisterVisit
    },
    '/pregister-visit/': {
        ...reportVisitTower
    },
    '/pregister-list-kiosk': {
        ...listPregisterVisitByKiosk
    },
    '/pregister-list-kiosk-report': {
        ...reportExcelListPregisterVisitByKiosk
    },
    '/generate-graphic-report-kiosk': {
        ...reportGraghicByKiosks
    }
}