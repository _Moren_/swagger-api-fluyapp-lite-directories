module.exports = {
    post: {
        tags: ['pregisters'],
        description: "Reporte en excel de cantidades de personas por torre",
        operationId: "reportVisitTower",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/reuqestBodyReportTowerExcel'
                    }
                }
            }
        },
        responses: {
            'API_PRG_200': {
                description: 'Listado de los registtados.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/responseBodyReportTowerExcel'
                        }
                    }
                }
            },
            'API_PRG_404': {
                description: 'No tiene registro previo.'
            },
            'API_PRG_500': {
                description: 'Ha ocurrido un error al mostrar los datos para este reporte.'
            }
        }
    }
}