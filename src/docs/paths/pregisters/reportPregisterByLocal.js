module.exports = {
    post: {
        tags: ['pregisters'],
        description: "Musestra todos los datos en base a fechas y nombre del local",
        operationId: "reportPregisterByLocal",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/reuqestBodyReportByLocal'
                    }
                }
            }
        },
        responses: {
            'API_PRG_200': {
                description:'Listado de los registrados.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/reponseBodyToReportToday'
                        }
                    }
                }
            },
            'API_PRG_403': {
                description: 'No hay regisros para la fecha actual.'
            },
            'API_PRG_500': {
                description: 'Ha ocurrido algo inesperado al generar el reporte.'
            }
        }
    }
}