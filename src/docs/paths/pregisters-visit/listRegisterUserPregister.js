module.exports = {
    post: {
        tags: ['pregisters-visits'],
        description: "Retorna una lista de todas las personas registradas",
        operationId: "listRegisterUserPregister",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/requestListingPregisterVisit'
                    }
                }
            }
        },
        responses: {
            'API_PRG_200': {
                description: 'Listado de los registados.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/responseListingPregisterVisit'
                        }
                    }
                }
            },

            'API_PRG_404': {
                description: 'No hay registros.'
            },
            'API_PRG_500': {
                description: 'Ha ocurrido un error al listar los registrados.'
            }
        }
    }
}