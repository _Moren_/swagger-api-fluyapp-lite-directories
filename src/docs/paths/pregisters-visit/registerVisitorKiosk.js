module.exports = {
    post: {
        tags: ['pregisters-visits'],
        description: "Registra los datos personales de una persona mediante el kiosco",
        operationId: "registerVisitorKiosk",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/createPregisterVisit'
                    }
                }
            }
        },
        responses: {
            'API_PRG_201': {
                description: 'Información general del pre-registrado.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/responseCreatePregisterVisit'
                        }
                    }
                }
            },
            'API_PRG_200': {
                description: 'Pre-registro creado con éxito..',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/responseCreatePregisterVisit'
                        }
                    }
                }
            },

            'API_PRG_202': {
                description: 'Ocurrió un error al registrarse. Intente de nuevo.'
            },
            'API_PRG_403': {
                description: 'Ha ocurrido un error en crear un preregistro.'
            }
        }
    }
}