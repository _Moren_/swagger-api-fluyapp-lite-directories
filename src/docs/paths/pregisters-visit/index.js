const registerVisitorKiosk = require('./registerVisitorKiosk');
const updateToBlackList = require('./updateToBlackList');
const updateNamePregister = require('./updateNamePregister');
const findPregister = require('./findPregister');
const listRegisterUserPregister = require('./listRegisterUserPregister');
const generateBlockedReportExcel = require('./generateBlockedReportExcel');

module.exports = {
    '/register-visitor-kiosk/': {
        ...registerVisitorKiosk
    },
    '/pregister-black-list/': {
        ...updateToBlackList
    },
    '/pregister-update-name/': {
        ...updateNamePregister
    },
    '/pregister-get-info': {
        ...findPregister
    },
    '/pregister-lists/': {
        ...listRegisterUserPregister
    },
    '/generate-blocked-report/': {
        ...generateBlockedReportExcel
    }
}