module.exports = {
    get: {
        tags: ['pregisters-visits'],
        description: "Busca información acerca de una persona",
        operationId: "findPregister",
        parameters: [
            {
                in: 'query',
                name: 'cedula'
            },
            {
                in: 'query',
                name: 'entityId'
            },
            {
                in: 'query',
                name: 'kioskoId'
            },
        ],
        responses: {
            'API_PRG_200': {
                description: 'Información general del pre-registrado.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/responseCreatePregisterVisit'
                        }
                    }
                }
            },
            'API_PRG_405': {
                description: 'Por favor, intente registrarse más tarde.'
            },
            'API_PRG_404': {
                description: 'No tiene registro previo.'
            },
            'API_PRG_406': {
                description: `Estimado/a ${'**nombrePersona**'}, su acceso a Torres de las Américas ha sido denegado temporalmente. Por favor, póngase en contacto con el personal de atención al cliente`
            },
            'API_PRG_500': {
                description: 'Ha ocurrido un error al buscar el pregistrado.'
            }
        }
    }
}