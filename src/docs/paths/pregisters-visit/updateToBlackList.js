module.exports = {
    put: {
        tags: ['pregisters-visits'],
        description: "Deshabilita o habilita el acceso a una persona al sitio",
        operationId: "updateToBlackList",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/requestBodyUpdateAccess'
                    }
                }
            }
        },
        responses: {
            'API_PRG_200': {
                description: 'Actualizado para la lista negra.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/responseUpdateBlockedPregisterVisit'
                        }
                    }
                }
            },

            'API_PRG_404': {
                description: 'No se pudo actualizar para la lista negra.'
            },
            'API_PRG_500': {
                description: 'Ha ocurrido un error al actualizar la lista negra.'
            }
        }
    }
}