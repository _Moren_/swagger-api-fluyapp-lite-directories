module.exports = {
    put: {
        tags: ['pregisters-visits'],
        description: "Actualiza el nombre de una persona",
        operationId: "updateNamePregister",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/requestBodyUpdateFullName'
                    }
                }
            }
        },
        responses: {
            'API_PRG_200': {
                description: 'Actualizado para la lista negra.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/responseUpdateFullNamePregisterVisit'
                        }
                    }
                }
            },

            'API_PRG_404': {
                description: 'No se pudo actualizar para la información.'
            },
            'API_PRG_500': {
                description: 'Ha ocurrido un error al actualizar los valores.'
            }
        }
    }
}