module.exports = {
    post: {
        tags: ['pregisters-visits'],
        description: "Retorna url con el documento en excel de las personas bloqueadas",
        operationId: "generateBlockedReportExcel",
        requestBody: {
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/requestBlockedPregisterVisit'
                    }
                }
            }
        },
        responses: {
            'API_PRG_200': {
                description: 'Reporte de personas bloqueadas.',
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/requestBlockedPregisterVisitReport'
                        }
                    }
                }
            },

            'API_PRG_403': {
                description: 'No se pudo generar el reporte. Intente de nuevo.',
            },
            'API_PRG_500': {
                description: 'Ha ocurrido un error al listar los bloqueados.'
            }
        }
    }
}