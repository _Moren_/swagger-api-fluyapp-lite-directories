module.exports = {
    openapi: "3.0.1",
    info: {
        version: "1.0.0",
        title: "Fluyapp Lite",
        description: "Fluyapp Lite API para el micro servicio de Directorios",
        contact: {
            email: "francisco@getxplor.com"
        }
    }
}