module.exports = {
    createCamera: {
        type: 'object',
        properties: {
            NameCamera: {
                type: 'string',
                description: 'nombre descriptivo para una cámara',
                example: 'Entrada 1'
            },
            idEntity: {
                type: 'number',
                description: 'identificador de la entidad',
                example: 36
            },
            type: {
                type: 'number',
                description: 'pendiente descripcion',
                example: 1
            },
            LinkCamera: {
                type: 'string',
                description: 'enlace para acceder a una cámara',
                example: '192.168.50.10'
            },
            IdsBranchs: {
                type: 'numberss',
                description: 'identificadores de sucursales',
                enum: [
                    1,
                    2,
                    3
                ]
            },
            branchId: {
                type: 'number',
                description: 'identificador de sucursales',
                example: '38'
            },
            username: {
                type: 'string',
                description: 'nombre de usuario para acceder a una cámara',
                example: 'admin'
            },
            password: {
                type: 'string',
                description: 'contraseña para acceder a una cámara',
                example: '1234565'
            },
            IdsDepartments: {
                type: 'number',
                description: 'identificadores de departamentos',
                enum: [
                    4,
                    5,
                    6
                ]
            },
            IdsServices: {
                type: 'number',
                description: 'identificadores de servicios',
                enum: [
                    7,
                    8,
                    9
                ]
            },
            CreatedDate: {
                type: 'string',
                description: 'fecha de creación',
                example: '25-Jun-2020 09:25:22 PM'
            },
            Updatedate: {
                type: 'string',
                description: 'fecha de actualizaci',
                example: '25-Jun-2020 09:25:22 PM'
            },

            CreatedBy: {
                type: 'number',
                description: 'quien creó el regisgtro',
                example: 1
            },
            UpdatedBy: {
                type: 'number',
                description: 'quien actualizó el regisgtro',
                example: 1
            },
        }
    },
    requestBodyCameraUpdated: {
        type: 'object',
        properties: {
            NameCamera: {
                type: 'string',
                description: 'nombre descriptivo para una cámara',
                example: 'Entrada 1'
            },
            LinkCamera: {
                type: 'string',
                description: 'enlace para acceder a una cámara',
                example: '192.168.50.10'
            },
            IdsBranchs: {
                type: 'numberss',
                description: 'identificadores de sucursales',
                enum: [
                    1,
                    2,
                    3
                ]
            },
            branchId: {
                type: 'number',
                description: 'identificador de sucursales',
                example: '38'
            },
            username: {
                type: 'string',
                description: 'nombre de usuario para acceder a una cámara',
                example: 'admin'
            },
            password: {
                type: 'string',
                description: 'contraseña para acceder a una cámara',
                example: '1234565'
            },
            Updatedate: {
                type: 'string',
                description: 'fecha de actualizaci',
                example: '25-Jun-2020 09:25:22 PM'
            },
            UpdatedBy: {
                type: 'number',
                description: 'quien actualizó el regisgtro',
                example: 1
            },
        }
    },
    requestBodyListsByBranchFlag: {
        type: 'object',
        properties: {
            id: {
                type: 'number',
                description: 'identificadores de: servicios, sucursales o departamentos',
                enum: [
                    1,
                    2,
                    3
                ]
            },
            flag: {
                type: 'string',
                description: 'identificador para generar la consulta',
                enum: [
                    'branch',
                    'service',
                    'departament'
                ]
            },
        }
    },
    requestBodyUpdateCamera: {
        type: 'object',
        properties: {
            id_: {
                type: 'number',
                description: 'identificadores de: servicios, sucursales o departamentos',
                enum: [
                    1,
                    2,
                    3
                ]
            },
            flag: {
                type: 'string',
                description: 'identificador para generar la consulta',
                enum: [
                    'branch',
                    'service',
                    'departament'
                ]
            },
            cameraId: {
                type: 'string',
                description: 'identificador de una cámara',
                example: '5efa1c22d3206f5b5c0ab74e'
            },
        }
    }
}