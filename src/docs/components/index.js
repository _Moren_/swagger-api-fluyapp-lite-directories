const cameras = require('./cameras-components/cameras-components');
const owners = require('./owners-components/owners-components');
const directories = require('./directories-components/directories-components');
const pregisters = require('./pregisters-components/pregisters-components');
const pregisters_visit = require('./pregisters-visti-components/pregisters-visti-components');

module.exports = {
    components: {
        schemas: {
            ...cameras,
            ...owners,
            ...directories,
            ...pregisters,
            ...pregisters_visit
        }
    }
}