module.exports = {
    createPregister: {
        type: 'object',
        properties: {
            entityId: {
                type: 'number',
                description: 'identificador de una entidad',
                example: 1
            },
            branchId: {
                type: 'number',
                description: 'identificador de una sucursal',
                example: 2
            },
            departamentId: {
                type: 'number',
                description: 'identificador de un departamento',
                example: 3
            },
            serviceId: {
                type: 'number',
                description: 'identificador de un servicio',
                example: 4
            },
            kioscoId: {
                type: 'number',
                description: 'identificador de un kiosco',
                example: 1
            },
            type_visit: {
                type: 'array',
                description: 'tipo de persona que accesa a los locales',
                example: '' //pendiente
            },
            person: {
                type: 'array',
                description: 'información de la persona que accesa a un local',
                example: {
                    "fullName": "ANDRES FELIPE OROZCO COLORADO ",
                    "cedula": "AO543955",
                    "email": "",
                    "phone": "",
                    "sexo": "",
                    "id": 105
                }
            },
            tower: {
                type: 'array',
                description: 'información acerca a donde se digire la persona',
                example: {
                    "id": 36,
                    "branchName": "Torre A",
                    "description": "Torre A",
                    "entityId": 36,
                    "maxDiffWaitTime": 10,
                    "maxWaitingTime": 10,
                    "criticalWaitingTime": 10,
                    "createBy": 37,
                    "status": null,
                    "updateBy": 37,
                    "timezone": "America/Panama",
                    "Banner": null,
                    "siteId": "5f04cb2653621630ef9046e3",
                    "latitud": 8.9971155,
                    "longitud": -79.5846823,
                    "createdAt": "2020-06-20T21:47:27.379Z",
                    "updatedAt": "2020-08-28T01:21:35.674Z"
                }
            },
            piso: {
                type: 'array',
                description: 'quien actualizó el regisgtro',
                example: {
                    "id": 77,
                    "departamentName": "Piso 14",
                    "description": "Piso 14",
                    "entityId": 36,
                    "createBy": 37,
                    "branchId": 36,
                    "updateBy": 37,
                    "state": null,
                    "createdAt": "2020-08-20T21:13:28.832Z",
                    "updatedAt": "2020-08-20T21:13:28.832Z"
                }
            },
            local: {
                type: 'array',
                description: 'información acerca del lugar específico que va la persona',
                example: {
                    "id": 181,
                    "serviceName": "MERCANTIL SEGUROS",
                    "branchId": 36,
                    "OfficeNumber": "1401",
                    "departamentId": 77
                }
            },
            photo: {
                type: 'string',
                description: 'fotografía de la persona',
                example: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAEBAQE'
            },
            CreatedDate: {
                type: 'string',
                description: 'fecha de creación',
                example: '2020-09-01T09:01:27.784Z'
            },
            Updatedate: {
                type: 'string',
                description: 'fecha de actualización',
                example: '2020-09-01T09:20:27.784Z'
            },
        }
    },
    requestBodyFaceRecognition: {
        type: 'object',
        properties: {
            cedula: {
                type: 'string',
                description: 'identificador único de una persona',
                example: '8-75-45'
            },
            photo: {
                type: 'string',
                description: 'fotografía de la persona',
                example: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAEBAQE'
            },
            entityId: {
                type: 'number',
                description: 'identificador de una entidad',
                example: 36
            },
        }
    },
    requestBodyToReportToday: {
        type: 'object',
        properties: {
            limit: {
                type: 'number',
                description: 'limite de páginas',
                example: 10
            },
            page: {
                type: 'number',
                description: 'número de página a lque se quiere ver',
                example: 1
            },
            entityId: {
                type: 'number',
                description: 'identificador de una entidad',
                example: 36
            },
        }
    },
    responseHistoryUser: {
        type: 'object',
        properties: {
            total: {
                type: 'number',
                description: 'total de documentos en la base de datos',
                example: 2
            },
            limit: {
                type: 'number',
                description: 'limite de páginas',
                example: 5
            },
            page: {
                type: 'number',
                description: 'número de página a lque se quiere ver',
                example: 1
            },
            persona: {

            },
            locales: {
                type: 'array',
                description: 'totales de locales que ha ido una persona',
                example: [
                    {
                        "local": "EVENTO PREGISTER",
                        "count": 2
                    }
                ]
            },
            pisos: {
                type: 'array',
                description: 'totales de pisos que ha ido una persona',
                example: [
                    {
                        "piso": "Piso 2",
                        "count": 2
                    }
                ]
            },
            towers: {
                type: 'array',
                description: 'totales de torees que ha ido una persona',
                example: [
                    {
                        "tower": "Torre A",
                        "count": 2
                    }
                ]
            },
            data: {
                type: 'array',
                description: 'variable que alberga toda la informacíón con respecto al reporte diario',
                example: {
                    "_id": "616f346d52386c002039bec9",
                    "entityId": 36,
                    "branchId": 36,
                    "serviceId": 50,
                    "departamentId": 41,
                    "person": {
                        "fullName": "JOSÉ ANTONIO MOLINA RAMÍREZ  ",
                        "cedula": "E-8-148446",
                        "email": "",
                        "phone": "",
                        "sexo": "M",
                        "id": 11522
                    },
                    "tower": {
                        "id": 36,
                        "branchName": "Torre A",
                        "description": "Torre A",
                        "entityId": 36,
                        "maxDiffWaitTime": 10,
                        "maxWaitingTime": 10,
                        "criticalWaitingTime": 10,
                        "createBy": 37,
                        "status": null,
                        "updateBy": 37,
                        "timezone": "America/Panama",
                        "Banner": null,
                        "isCapturePhoto": true,
                        "siteId": "5f04cb2653621630ef9046e3",
                        "latitud": 8.9971155,
                        "longitud": -79.5846823,
                        "createdAt": "2020-06-20T21:47:27.379Z",
                        "updatedAt": "2020-09-07T19:27:16.276Z"
                    },
                    "piso": {
                        "id": 41,
                        "departamentName": "Piso 2",
                        "description": "Piso 2",
                        "entityId": 36,
                        "createBy": 37,
                        "branchId": 36,
                        "updateBy": 37,
                        "state": null,
                        "createdAt": "2020-06-22T21:38:07.114Z",
                        "updatedAt": "2020-08-20T20:30:48.438Z"
                    },
                    "local": {
                        "id": 50,
                        "serviceName": "EVENTO PREGISTER",
                        "branchId": 36,
                        "OfficeNumber": "204",
                        "departamentId": 41
                    },
                    "type_visit": {
                        "id": 1,
                        "tipo_visita": "Visitantes",
                        "createdAt": "2020-10-30T23:59:46.388Z",
                        "updatedAt": "2020-10-30T23:59:46.388Z"
                    },
                    "kioscoId": 1,
                    "CreatedDate": "2021-10-19T16:11:09.694Z",
                    "Updatedate": "2021-10-19T16:11:09.694Z",
                    "__v": 0
                }
            }
        }
    },
    reponseBodyToReportToday: {
        type: 'object',
        properties: {
            total: {
                type: 'number',
                description: 'total de documentos en la base de datos',
                example: 10
            },
            limit: {
                type: 'number',
                description: 'limite de páginas',
                example: 10
            },
            page: {
                type: 'number',
                description: 'número de página a lque se quiere ver',
                example: 1
            },
            data: {
                type: 'array',
                description: 'variable que alberga toda la informacíón con respecto al reporte diario',
                example: {
                    "_id": "616ec5e452386c002039bec0",
                    "entityId": 36,
                    "branchId": 36,
                    "serviceId": 50,
                    "departamentId": 41,
                    "person": {
                        "fullName": "JAIRO JOSUE JAEN UREÑA",
                        "cedula": "8-908-2170",
                        "email": "",
                        "phone": "",
                        "sexo": "M",
                        "id": 11519
                    },
                    "tower": {
                        "id": 36,
                        "branchName": "Torre A",
                        "description": "Torre A",
                        "entityId": 36,
                        "maxDiffWaitTime": 10,
                        "maxWaitingTime": 10,
                        "criticalWaitingTime": 10,
                        "createBy": 37,
                        "status": null,
                        "updateBy": 37,
                        "timezone": "America/Panama",
                        "Banner": null,
                        "isCapturePhoto": true,
                        "siteId": "5f04cb2653621630ef9046e3",
                        "latitud": 8.9971155,
                        "longitud": -79.5846823,
                        "createdAt": "2020-06-20T21:47:27.379Z",
                        "updatedAt": "2020-09-07T19:27:16.276Z"
                    },
                    "piso": {
                        "id": 41,
                        "departamentName": "Piso 2",
                        "description": "Piso 2",
                        "entityId": 36,
                        "createBy": 37,
                        "branchId": 36,
                        "updateBy": 37,
                        "state": null,
                        "createdAt": "2020-06-22T21:38:07.114Z",
                        "updatedAt": "2020-08-20T20:30:48.438Z"
                    },
                    "local": {
                        "id": 50,
                        "serviceName": "EVENTO PREGISTER",
                        "branchId": 36,
                        "OfficeNumber": "204",
                        "departamentId": 41
                    },
                    "type_visit": {
                        "id": 1,
                        "tipo_visita": "Visitantes",
                        "createdAt": "2020-10-30T23:59:46.388Z",
                        "updatedAt": "2020-10-30T23:59:46.388Z"
                    },
                    "kioscoId": 1,
                    "CreatedDate": "2021-10-19T08:19:32.744Z",
                    "Updatedate": "2021-10-19T08:19:32.744Z",
                    "__v": 0,
                    "dayOfWeek": "Martes"
                }
            }
        }
    },
    requestBodyToReportTodayExcel: {
        type: 'object',
        properties: {
            entityId: {
                type: 'number',
                description: 'identificador de una entidad',
                example: 36
            },
        }
    },
    responseBodyToReportTodayExcel: {
        type: 'object',
        properties: {
            name_file: {
                type: 'string',
                description: 'enlace que contiene el archivo excel a ser descargado',
                example: 'https://cdn.getxplor.net/Fluyapp/pregister/e9d42b40-950d-4658-97e8-54eb5ad27ba4.xlsx'
            },
        }
    },
    reuqestBodyReportByLocal: {
        type: 'object',
        properties: {
            startDate: {
                type: 'string',
                description: 'fecha inicial',
                example: '2021-10-01'
            },
            endDate: {
                type: 'string',
                description: 'fecha final',
                example: '2021-10-31'
            },
            serviceName: {
                type: 'string',
                description: 'nombre del local',
                example: 'EVENTO PREGISTER'
            },
        }
    },
    reuqestBodyReportTowerExcel: {
        type: 'object',
        properties: {
            startDate: {
                type: 'string',
                description: 'fecha inicial',
                example: '2021-10-01'
            },
            endDate: {
                type: 'string',
                description: 'fecha final',
                example: '2021-10-31'
            },
            type: {
                type: 'string',
                description: 'tipo de administrador',
                example: 'byAdminXplor'
            },
            entityId: {
                type: 'number',
                description: 'identificador de una entidad',
                example: 36
            },
        }
    },
    responseBodyReportTowerExcel: {
        type: 'object',
        properties: {
            name_file: {
                type: 'string',
                description: 'url para descargar el excel',
                example: 'https://cdn.getxplor.net/Fluyapp/pregister/5df52b11-22f6-4d85-8997-abb4c2b5f8ec.xlsx'
            },
            data: {
                type: 'array',
                description: 'contiene todos los valores sobre el conteo de entrada a las torres',
                example: [
                    {
                        "branchId": [
                            36
                        ],
                        "label": "Torre A",
                        "count": 2465
                    },
                    {
                        "branchId": [
                            42
                        ],
                        "label": "FOOD COURT",
                        "count": 58
                    },
                    {
                        "branchId": [
                            38
                        ],
                        "label": "Torre C",
                        "count": 2817
                    },
                    {
                        "branchId": [
                            41
                        ],
                        "label": "MEZZANNINE",
                        "count": 38
                    },
                    {
                        "branchId": [
                            39
                        ],
                        "label": "Torre D",
                        "count": 33
                    },
                    {
                        "branchId": [
                            37
                        ],
                        "label": "Torre B",
                        "count": 2623
                    },
                    {
                        "branchId": [
                            40
                        ],
                        "label": "ATRIO",
                        "count": 141
                    }
                ]
            }
        }
    },
    reuqestBodyReportLocalByDate: {
        type: 'object',
        properties: {
            startDate: {
                type: 'string',
                description: 'fecha inicial',
                example: '2021-10-01'
            },
            endDate: {
                type: 'string',
                description: 'fecha final',
                example: '2021-10-31'
            },
            limit: {
                type: 'number',
                description: 'limite de páginas',
                example: 10
            },
            selectedPage: {
                type: 'number',
                description: 'número de página a lque se quiere ver',
                example: 1
            },
            entityId: {
                type: 'number',
                description: 'identificador de una entidad',
                example: 36
            },
        }
    },
    reuqestBodyReportLocalByDateExcel: {
        type: 'object',
        properties: {
            startDate: {
                type: 'string',
                description: 'fecha inicial',
                example: '2021-10-01'
            },
            endDate: {
                type: 'string',
                description: 'fecha final',
                example: '2021-10-31'
            },
            entityId: {
                type: 'number',
                description: 'identificador de una entidad',
                example: 36
            },
        }
    },
    responsePhoto: {
        type: 'object',
        properties: {
            photo: {
                type: 'string',
                description: 'imagen de la persona',
                example: 'https://cdn.getxplor.net/Fluyapp/TorresAmericas/22ac051e-43f6-43df-b78b-09fbc7c70b93.jpg'
            },
        }
    },
    requestIdPhoto: {
        type: 'object',
        properties: {
            id: {
                type: 'string',
                description: 'identificador de la persona',
                example: '616f346d52386c002039bec9'
            },
        }
    },
    requestBodyUserHistory: {
        type: 'object',
        properties: {
            limit: {
                type: 'number',
                description: 'limite de páginas',
                example: 10
            },
            page: {
                type: 'number',
                description: 'número de página a lque se quiere ver',
                example: 1
            },
            entityId: {
                type: 'number',
                description: 'identificador de una entidad',
                example: 36
            },
            cedula: {
                type: 'string',
                description: 'identificador de una persona',
                example: 'E-8-148446'
            },
        }
    },
    requestBodyVisitKiosk: {
        type: 'object',
        properties: {
            startDate: {
                type: 'string',
                description: 'fecha inicial',
                example: '2021-10-01'
            },
            endDate: {
                type: 'string',
                description: 'fecha final',
                example: '2021-10-31'
            },
            limit: {
                type: 'number',
                description: 'limite de páginas',
                example: 10
            },
            page: {
                type: 'number',
                description: 'número de página a lque se quiere ver',
                example: 1
            },
            entityId: {
                type: 'number',
                description: 'identificador de una entidad',
                example: 36
            },
            kioscoIds: {
                type: 'array',
                description: 'contiene los identifocadores de los kioscos',
                example: [1, 4, 5]
            }
        }
    },
    responseBodyVisitKiosk: {
        type: 'object',
        properties: {
            limit: {
                type: 'number',
                description: 'limite de páginas',
                example: 10
            },
            page: {
                type: 'number',
                description: 'número de página a lque se quiere ver',
                example: 1
            },
            total: {
                type: 'number',
                description: 'total de documentos en la base de datos',
                example: 10
            },
            data: {
                type: 'array',
                description: 'almacena toda la información para el reporte por kiosco',
                example: {
                    data: [
                        {
                            "_id": "6170765c9f92fa002bec1d7d",
                            "entityId": 36,
                            "branchId": 36,
                            "serviceId": 97,
                            "departamentId": 69,
                            "person": {
                                "fullName": "CARLOS HUMBERTO ORO ALONZO",
                                "cedula": "8-754-735",
                                "email": "",
                                "phone": "",
                                "sexo": "M",
                                "id": 11507
                            },
                            "tower": {
                                "id": 36,
                                "branchName": "Torre A",
                                "description": "Torre A",
                                "entityId": 36,
                                "createBy": 37,
                                "updateBy": 37
                            },
                            "piso": {
                                "id": 69,
                                "departamentName": "Piso 5",
                                "description": "Piso 5",
                                "entityId": 36,
                                "branchId": 36
                            },
                            "local": {
                                "id": 97,
                                "serviceName": "THE BANK OF NOVA SCOTIA /SCOTIABANK",
                                "branchId": 36,
                                "OfficeNumber": "501",
                                "departamentId": 69
                            },
                            "type_visit": {
                                "id": 2,
                                "tipo_visita": "Proveedores"
                            },
                            "kioscoId": 1,
                            "CreatedDate": "2021-10-20T15:04:44.020Z",
                            "__v": 0,
                            "dayOfWeek": "Miercoles"
                        },
                    ]
                }
            }
        }
    },
    requestBodyVisitKiosk: {
        type: 'object',
        properties: {
            startDate: {
                type: 'string',
                description: 'fecha inicial',
                example: '2021-10-01'
            },
            endDate: {
                type: 'string',
                description: 'fecha final',
                example: '2021-10-31'
            },
            entityId: {
                type: 'number',
                description: 'identificador de una entidad',
                example: 36
            },
            kioscoIds: {
                type: 'array',
                description: 'contiene los identifocadores de los kioscos',
                example: [1, 4, 5]
            }
        }
    },
    responseBodyVisitKiosk: {
        type: 'object',
        properties: {
            name_file: {
                type: 'string',
                description: 'url que contiene el reporte el excel',
                example: 'https://cdn.getxplor.net/Fluyapp/pregister/c0e4a24a-39a5-425c-b1cf-f077bdcc0456.xlsx'
            }
        }
    },
    requestBodyVisitKioskGraphic: {
        type: 'object',
        properties: {
            startDate: {
                type: 'string',
                description: 'fecha inicial',
                example: '2021-10-01'
            },
            endDate: {
                type: 'string',
                description: 'fecha final',
                example: '2021-10-31'
            },
            entityId: {
                type: 'number',
                description: 'identificador de una entidad',
                example: 36
            }
        }
    },
    responseBodyVisitKioskGraphic: {
        type: 'object',
        properties: {
            data: {
                type: 'array',
                description: 'donde se almacena toda la información para generar la gráfica',
                example: [
                    {
                        "total": 142,
                        "nameKiosk": "Torre A Kiosco 4"
                    },
                    {
                        "total": 109,
                        "nameKiosk": "Torre A Kiosco 2"
                    },
                    {
                        "total": 106,
                        "nameKiosk": ""
                    },
                    {
                        "total": 85,
                        "nameKiosk": "Torre B Kiosco 3"
                    },
                    {
                        "total": 47,
                        "nameKiosk": "Area de Carga"
                    },
                    {
                        "total": 77,
                        "nameKiosk": "Torre B Kiosco 1"
                    }
                ]
            },
            name_file: {
                type: 'string',
                description: 'url para descargar el reporte en excel',
               example: 'https://cdn.getxplor.net/Fluyapp/pregister/3c1ce92b-31ca-4e0c-9697-cb12359ea752.xlsx'
            }
        }
    }
}