module.exports = {
    createOwner: {
        type: 'object',
        properties: {
            entityId: {
                type: 'number',
                description: 'identificador de una entidad',
                example: 1
            },
            branchId: {
                type: 'number',
                description: 'identificador de una sucursal',
                example: 2
            },
            serviceId: {
                type: 'number',
                description: 'identificador de un servicio',
                example: 3
            },
            dataMixed: {
                type: 'object',
                description: 'enlace para acceder a una cámara',
                example: {
                    "branch": {
                        "branchId": 3,
                        "BranchName": "FOOD COURT"
                    },
                    "departament": {
                        "departamentId": 3,
                        "DepartamentName": "Piso 3"
                    },
                    "service": {
                        "serviceId": "43",
                        "ServiceName": "BUSQUEDA EFECTIVA / COLAN HIGHLAMD GROUP"
                    }
                }
            },
            NameOwner: {
                type: 'string',
                description: 'nombre del propietario',
                example: 'Usuario A'
            },
            emailOnwer: {
                type: 'string',
                description: 'acerca de lo que hace el propietario',
                example: 'prueba descripción'
            },
            Description: {
                type: 'string',
                description: 'nombre de usuario para acceder a una cámara',
                example: 'admin'
            },
            telefono: {
                type: 'string',
                description: 'numero de contacto',
                example: '6277-4449'
            },
            CreatedDate: {
                type: 'string',
                description: 'fecha de creación',
                example: '2021-06-25T10:15:41.014+00:00'
            },
            Updatedate: {
                type: 'string',
                description: 'fecha de actualización',
                example: '2021-06-27T10:15:41.014+00:00'
            },
            CreatedBy: {
                type: 'number',
                description: 'quien creó el regisgtro',
                example: 1
            },
            UpdatedBy: {
                type: 'number',
                description: 'quien actualizó el regisgtro',
                example: 1
            },
            Status: {
                type: 'boolean',
                description: 'habilitar o deshabilitar un propietario',
                example: true
            },
        }
    },
    requestBodyOwnerUpdate: {
        type: 'object',
        properties: {
            entityId: {
                type: 'number',
                description: 'identificador de una entidad',
                example: 1
            },
            branchId: {
                type: 'number',
                description: 'identificador de una sucursal',
                example: 2
            },
            serviceId: {
                type: 'number',
                description: 'identificador de un servicio',
                example: 3
            },
            dataMixed: {
                type: 'object',
                description: 'enlace para acceder a una cámara',
                example: {
                    "branch": {
                        "branchId": 3,
                        "BranchName": "FOOD COURT"
                    },
                    "departament": {
                        "departamentId": 3,
                        "DepartamentName": "Piso 3"
                    },
                    "service": {
                        "serviceId": "43",
                        "ServiceName": "BUSQUEDA EFECTIVA / COLAN HIGHLAMD GROUP"
                    }
                }
            },
            NameOwner: {
                type: 'string',
                description: 'nombre del propietario',
                example: 'Usuario A'
            },
            emailOnwer: {
                type: 'string',
                description: 'acerca de lo que hace el propietario',
                example: 'prueba descripción'
            },
            Description: {
                type: 'string',
                description: 'nombre de usuario para acceder a una cámara',
                example: 'admin'
            },
            telefono: {
                type: 'string',
                description: 'numero de contacto',
                example: '6277-4449'
            },
            UpdatedBy: {
                type: 'number',
                description: 'quien actualizó el regisgtro',
                example: 1
            },
            Status: {
                type: 'boolean',
                description: 'habilitar o deshabilitar un propietario',
                example: true
            },
        }
    }
}