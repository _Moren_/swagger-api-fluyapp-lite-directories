module.exports = {
    createPregisterVisit: {
        type: 'object',
        properties: {
            entityId: {
                type: 'number',
                description: 'identificador de una entidad',
                example: 1
            },
            fullName: {
                type: 'string',
                description: 'nombre completo de una persona',
                example: 'Oscar Bruce Ramirez Jimenez'
            },
            cedula: {
                type: 'string',
                description: 'identificador de una persona',
                example: '8-79-804'
            },
            email: {
                type: 'string',
                description: 'correo electrónico de una persona',
                example: 'example@gmail.com'
            },
            phone: {
                type: 'string',
                description: 'nuḿero telefónico de  una persona',
                example: '6693-5423'
            },

            sexo: {
                type: 'string',
                description: 'género de la persona',
                example: 'F | M'
            },
            photo: {
                type: 'string',
                description: 'imagen de una persona',
                example: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQE'
            },
        }
    },
    responseCreatePregisterVisit: {
        type: 'object',
        properties: {
            id: {
                type: 'number',
                description: 'identificador autogenerado para ser usado por las cámaras',
                example: 50
            },
            entityId: {
                type: 'number',
                description: 'identificador de una entidad',
                example: 1
            },
            fullName: {
                type: 'string',
                description: 'nombre completo de una persona',
                example: 'Oscar Bruce Ramirez Jimenez'
            },
            cedula: {
                type: 'string',
                description: 'identificador de una persona',
                example: '8-79-804'
            },
            email: {
                type: 'string',
                description: 'correo electrónico de una persona',
                example: 'example@gmail.com'
            },
            phone: {
                type: 'string',
                description: 'nuḿero telefónico de  una persona',
                example: '6693-5423'
            },
            sexo: {
                type: 'string',
                description: 'género de la persona',
                example: 'M'
            },
            lastDateAccess: {
                type: 'string',
                description: 'último acceso que tuvo la persona',
                example: ''
            },
            photo: {
                type: 'string',
                description: 'imagen de una persona',
                example: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQE'
            },
            updateBlocked: {
                type: 'boolean',
                description: 'nuḿero telefónico de  una persona',
                example: '6693-5423'
            },
        }
    },
    requestBodyUpdateAccess: {
        type: 'object',
        properties: {
            cedula: {
                type: 'string',
                description: 'identificador de una persona',
                example: '8-79-804'
            },
            _id: {
                type: 'string',
                description: 'identificador generado por la base de datos',
                example: '6170a2e3cf9696002b04910c'
            },
            isBlocked: {
                type: 'boolean',
                description: 'variable para bloquea o desbloquea el acceso a una persona',
                example: true
            },
        }
    },
    responseUpdateBlockedPregisterVisit: {
        type: 'object',
        properties: {
            id: {
                type: 'number',
                description: 'identificador autogenerado para ser usado por las cámaras',
                example: 50
            },
            entityId: {
                type: 'number',
                description: 'identificador de una entidad',
                example: 1
            },
            fullName: {
                type: 'string',
                description: 'nombre completo de una persona',
                example: 'Oscar Bruce Ramirez Jimenez'
            },
            cedula: {
                type: 'string',
                description: 'identificador de una persona',
                example: '8-79-804'
            },
            email: {
                type: 'string',
                description: 'correo electrónico de una persona',
                example: 'example@gmail.com'
            },
            phone: {
                type: 'string',
                description: 'nuḿero telefónico de  una persona',
                example: '6693-5423'
            },
            sexo: {
                type: 'string',
                description: 'género de la persona',
                example: 'M'
            },
            lastDateAccess: {
                type: 'string',
                description: 'último acceso que tuvo la persona',
                example: ''
            },
            photo: {
                type: 'string',
                description: 'imagen de una persona',
                example: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQE'
            },
            updateBlocked: {
                type: 'boolean',
                description: 'nuḿero telefónico de  una persona',
                example: '6693-5423'
            },
            isBlocked: {
                type: 'boolean',
                description: 'variable para bloquea o desbloquea el acceso a una persona',
                example: true
            },
        }
    },
    requestBodyUpdateFullName: {
        type: 'object',
        properties: {
            cedula: {
                type: 'string',
                description: 'identificador de una persona',
                example: '8-79-804'
            },
            _id: {
                type: 'string',
                description: 'identificador generado por la base de datos',
                example: '6170a2e3cf9696002b04910c'
            },
            fullName: {
                type: 'string',
                description: 'variable para bloquea o desbloquea el acceso a una persona',
                example: 'Daniel Audilio Gonzalez Gonzalez'
            },
        }
    },
    responseUpdateFullNamePregisterVisit: {
        type: 'object',
        properties: {
            id: {
                type: 'number',
                description: 'identificador autogenerado para ser usado por las cámaras',
                example: 50
            },
            entityId: {
                type: 'number',
                description: 'identificador de una entidad',
                example: 1
            },
            fullName: {
                type: 'string',
                description: 'nombre completo de una persona',
                example: 'Daniel Audilio Gonzalez Gonzalez'
            },
            cedula: {
                type: 'string',
                description: 'identificador de una persona',
                example: '8-79-804'
            },
            email: {
                type: 'string',
                description: 'correo electrónico de una persona',
                example: 'example@gmail.com'
            },
            phone: {
                type: 'string',
                description: 'nuḿero telefónico de  una persona',
                example: '6693-5423'
            },
            sexo: {
                type: 'string',
                description: 'género de la persona',
                example: 'M'
            },
            lastDateAccess: {
                type: 'string',
                description: 'último acceso que tuvo la persona',
                example: ''
            },
            photo: {
                type: 'string',
                description: 'imagen de una persona',
                example: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQE'
            },
            updateBlocked: {
                type: 'boolean',
                description: 'nuḿero telefónico de  una persona',
                example: '6693-5423'
            },
            isBlocked: {
                type: 'boolean',
                description: 'variable para bloquea o desbloquea el acceso a una persona',
                example: true
            },
        }
    },
    requestListingPregisterVisit: {
        type: 'object',
        properties: {
            limit: {
                type: 'number',
                description: 'limite de valores a mostrar',
                example: 10
            },
            entityId: {
                type: 'number',
                description: 'identificador de una entidad',
                example: 1
            },
            selectedPage: {
                type: 'number',
                description: 'número de páginas a mostrar',
                example: 2
            },
            search: {
                type: 'string',
                description: 'identificador de una persona. Si esta variable está vacía, retornará todos los valores.',
                example: ['fullName', 'cedula', 'phone', 'email']
            },
        }
    },
    responseListingPregisterVisit: {
        type: 'object',
        properties: {
            total: {
                type: 'number',
                description: 'cantidad total de registeos',
                example: 100
            },
            limit: {
                type: 'number',
                description: 'limite de valores a mostrar',
                example: 10
            },
            entityId: {
                type: 'number',
                description: 'identificador de una entidad',
                example: 1
            },
            page: {
                type: 'number',
                description: 'número de páginas a mostrar',
                example: 2
            },
            data: {
                type: 'array',
                description: 'variable que contiene todos los valores de la busqueda',
                example: [
                    {
                        "_id": "617185039f92fa002bec1ef4",
                        "entityId": 36,
                        "fullName": "Ines Maria Zanetti Amado  ",
                        "cedula": "8-720-2448",
                        "email": "",
                        "phone": "",
                        "sexo": "F",
                        "lastDateAccess": "2021-10-21T10:19:58.339Z",
                        "CreatedDate": "2021-10-21T10:19:31.083Z",
                        "Updatedate": "2021-10-21T10:19:31.083Z",
                        "updateBlocked": "2021-10-21T10:19:31.083Z",
                        "id": 29831,
                        "__v": 0
                    },
                    {
                        "_id": "617184f49f92fa002bec1ef3",
                        "entityId": 36,
                        "fullName": "FELIX GOMEZ BOSQUEZ",
                        "cedula": "8-739-1839",
                        "email": "",
                        "phone": "",
                        "sexo": "",
                        "lastDateAccess": "2021-10-21T10:19:40.453Z",
                        "CreatedDate": "2021-10-21T10:19:16.167Z",
                        "Updatedate": "2021-10-21T10:19:16.167Z",
                        "updateBlocked": "2021-10-21T10:19:16.167Z",
                        "id": 29830,
                        "__v": 0
                    },
                    {
                        "_id": "617184f09f92fa002bec1ef2",
                        "entityId": 36,
                        "fullName": "Eneida  Orocu Arauz  ",
                        "cedula": "4-250-907",
                        "email": "",
                        "phone": "",
                        "sexo": "F",
                        "lastDateAccess": "2021-10-21T10:19:52.908Z",
                        "CreatedDate": "2021-10-21T10:19:12.095Z",
                        "Updatedate": "2021-10-21T10:19:12.095Z",
                        "updateBlocked": "2021-10-21T10:19:12.095Z",
                        "id": 29829,
                        "__v": 0
                    },
                    {
                        "_id": "6171841d9f92fa002bec1eeb",
                        "entityId": 36,
                        "fullName": "Ingree Karilin Ponce Miranda",
                        "cedula": "8-775-2494",
                        "email": "",
                        "phone": "",
                        "sexo": "F",
                        "lastDateAccess": "2021-10-21T10:16:26.290Z",
                        "CreatedDate": "2021-10-21T10:15:41.705Z",
                        "Updatedate": "2021-10-21T10:15:41.705Z",
                        "updateBlocked": "2021-10-21T10:15:41.705Z",
                        "id": 29828,
                        "__v": 0
                    },
                ]
            }
        }
    },
    requestBlockedPregisterVisit: {
        type: 'object',
        properties: {
            entityId: {
                type: 'number',
                description: 'identificador de una entidad',
                example: 1
            },
            isBlocked: {
                type: 'boolean',
                description: 'variable para bloquea o desbloquea el acceso a una persona',
                example: true
            },
        }
    },
    requestBlockedPregisterVisitReport: {
        type: 'object',
        properties: {
            name_file: {
                type: 'string',
                description: 'url con el reporte en excel',
                example: 'https://cdn.getxplor.net/Fluyapp/pregister/2b3f54b5-aba8-451b-9390-e8b74e41f889.xlsx'
            }
        }
    }
}