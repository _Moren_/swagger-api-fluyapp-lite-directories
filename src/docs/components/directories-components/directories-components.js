module.exports = {
    createCamera: {
        type: 'object',
        properties: {
            imageBase64: {
                type: 'string',
                description: 'imagen para ser usada en los kioscos',
                example: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFEAAABeCAYAAABb2fjjAAAU'
            },
            idEntity: {
                type: 'number',
                description: 'identificador para las sucursales',
                example: 'branch'
            },
            flag: {
                type: 'string',
                description: 'identificador de la entidad',
                example: 36
            },
            idRefer: {
                type: 'number',
                description: 'referencia de id de  una sucursal',
                example: 40
            },
            imageOverBase64: {
                type: 'string',
                description: 'imagen para ser usada en los kioscos',
                example: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFEAAABeCAYAAABb2fjjAAAV'
            },
            CreatedDate: {
                type: 'string',
                description: 'fecha de creación',
                example: '25-Jun-2020 09:25:22 PM'
            },
            Updatedate: {
                type: 'string',
                description: 'fecha de actualizaci',
                example: '25-Jun-2020 09:25:22 PM'
            },

            CreatedBy: {
                type: 'number',
                description: 'quien creó el regisgtro',
                example: 1
            },
            UpdatedBy: {
                type: 'number',
                description: 'quien actualizó el regisgtro',
                example: 1
            },
            Status: {
                type: 'boolean',
                description: 'habilita o dehabilita un directorio',
                example: true
            },
        }
    },
    responseListDirectories:{
        type: 'object',
        properties: {
            imageBase64: {
                type: 'string',
                description: 'imagen para ser usada en los kioscos',
                example: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFEAAABeCAYAAABb2fjjAAAU'
            },
            _id: {
                type: 'string',
                description: 'identificador de un directorio',
                example: '5ef4e80feae9b515f8adb5c7'
            },
            idRefer: {
                type: 'number',
                description: 'referencia de id de  una sucursal',
                example: 40
            },
            imageOverBase64: {
                type: 'string',
                description: 'imagen para ser usada en los kioscos',
                example: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFEAAABeCAYAAABb2fjjAAAV'
            },
        }
    },
    requestBodyUpdate:{
        type: 'object',
        properties: {
            sourceImg: {
                type: 'string',
                description: 'imagen para ser usada en los kioscos',
                example: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFEAAABeCAYAAABb2fjjAAAU'
            },
            idEntity: {
                type: 'number',
                description: 'identificador para las sucursales',
                example: 'branch'
            },
            flag: {
                type: 'string',
                description: 'identificador de la entidad',
                example: 36
            },
            Updatedate: {
                type: 'string',
                description: 'fecha de actualizaci',
                example: '25-Jun-2020 09:25:22 PM'
            },
            UpdatedBy: {
                type: 'number',
                description: 'quien actualizó el regisgtro',
                example: 1
            },
            Status: {
                type: 'boolean',
                description: 'habilita o dehabilita un directorio',
                example: true
            },
        }
    }
}