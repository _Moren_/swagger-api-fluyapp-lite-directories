const { API_FLUYAPP_LITE_DEV } = require('../../config');
module.exports = {
    servers: [
        {
            url: API_FLUYAPP_LITE_DEV,
            description: "Api server-test fluyapp-lite"
        },
        {
            url: 'localhost:8002',
            description: "Local server fluyapp-lite"
        }
    ]
}